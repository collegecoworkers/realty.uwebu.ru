<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project_order`.
 */
class m170124_021633_create_project_order_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_order', [
            'id' => $this->primaryKey(),
            'project_id'=>$this->integer(),
            'order_id'=>$this->integer()
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'order_project_project_id',
            'project_order',
            'project_id'
        );


        // add foreign key for table `user`
        $this->addForeignKey(
            'order_project_project_id',
            'project_order',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx_order_id',
            'project_order',
            'order_id'
        );


        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-order_id',
            'project_order',
            'order_id',
            'order',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('project_order');
    }
}
