<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order`.
 */
class m170124_021608_create_order_table extends Migration {
	/**
	 * @inheritdoc
	 */
	public function up() {
		$this->createTable('order', [
			'id' => $this->primaryKey(),
			'user_id'=>$this->integer(),
			'project_id'=>$this->integer(),
			'email'=>$this->string(),
			'phone'=>$this->string(),
			'date'=>$this->date(),
		]);

		// creates index for column `user_id`
		$this->createIndex(
			'idx-post-user_id',
			'order',
			'user_id'
		);

		// add foreign key for table `user`
		$this->addForeignKey(
			'fk-post-user_id',
			'order',
			'user_id',
			'user',
			'id',
			'CASCADE'
		);


		// creates index for column `project_id`
		$this->createIndex(
			'idx-project_id',
			'order',
			'project_id'
			);

		// add foreign key for table `project`
		$this->addForeignKey(
			'fk-project_id',
			'order',
			'project_id',
			'project',
			'id',
			'CASCADE'
			);
	}

	/**
	 * @inheritdoc
	 */
	public function down() {
		$this->dropTable('order');
	}
}
