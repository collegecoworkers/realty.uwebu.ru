<?php

namespace app\models;

use Yii;
use app\models\User;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $title
 *
 * @property ProjectOrder[] $projectOrders
 */
class Order extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'order';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['email', 'phone'], 'required'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'UserName' => 'Пользоваетль',
			'ProjectTitle' => 'Проект',
			'email' => 'Почта',
			'phone' => 'Телефон',
			'date' => 'Дата',
		];
	}

	public function getProjects()
	{
		return $this->hasMany(Project::className(), ['id' => 'project_id'])
			->viaTable('project_order', ['order_id' => 'id']);
	}

	public function getByProject($project_id)
	{
		return Order::find()->where(['project_id' => $project_id])->all();
	}

	public function getUserName(){
		return User::findIdentity($this->id)->name;
	}

	public function getProjectTitle(){
		return Project::findIdentity($this->id)->title;
	}
}
