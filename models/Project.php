<?php

namespace app\models;

use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

class Project extends \yii\db\ActiveRecord
{
	public static function tableName()
	{
		return 'project';
	}

	public function rules()
	{
		return [
			[['title', 'price', 'address'], 'required'],
			[['title','description','content'], 'string'],
			[['title'], 'string', 'max' => 255],
			[['address'], 'string', 'max' => 512],
			[['date'], 'date', 'format'=>'php:Y-m-d'],
			[['date'], 'default', 'value' => date('Y-m-d')],
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Заголовок',
			'description' => 'Описание',
			'content' => 'Контент',
			'date' => 'Дата создания',
			'price' => 'Стоимость',
			'address' => 'Адрес',
			'image' => 'Изображение',
			'viewed' => 'Просмотров',
			'user_id' => 'ID пользователя',
			'status' => 'Статус',
		];
	}
	
	public static function findIdentity($id)
	{
		return Project::findOne($id);
	}

	public function saveProject()
	{
		$this->user_id = Yii::$app->user->id;
		if(!$this->date) $this->date = date('Y-m-d');
		return $this->save(false);
	}

	public function saveImage($filename)
	{
		$this->image = $filename;
		return $this->save(false);
	}

	public function getImage()
	{
		return ($this->image) ? '/uploads/' . $this->image : 'https://placehold.it/500x500';
	}

	public function deleteImage()
	{
		$imageUploadModel = new ImageUpload();
		$imageUploadModel->deleteCurrentImage($this->image);
	}

	public function beforeDelete()
	{
		$this->deleteImage();
		return parent::beforeDelete(); // TODO: Change the autogenerated stub
	}

	public function getOrders()
	{
		return $this->hasMany(Order::className(), ['id' => 'order_id'])
			->viaTable('project_order', ['project_id' => 'id']);
	}
	
	public function getSelectedOrders()
	{
		 $selectedIds = $this->getOrders()->select('id')->asArray()->all();
		return ArrayHelper::getColumn($selectedIds, 'id');
	}

	public function saveOrders($orders)
	{
		if (is_array($orders))
		{
			$this->clearCurrentOrders();

			foreach($orders as $order_id)
			{
				$order = Order::findOne($order_id);
				$this->link('orders', $order);
			}
		}
	}

	public function clearCurrentOrders()
	{
		ProjectOrder::deleteAll(['project_id'=>$this->id]);
	}
	
	public function getDate()
	{
		return Yii::$app->formatter->asDate($this->date);
	}
	
	public static function getAll($pageSize = 5)
	{
		// build a DB query to get all projects
		$query = Project::find();

		// get the total number of projects (but do not fetch the project data yet)
		$count = $query->count();

		// create a pagination object with the total count
		$pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);

		// limit the query using the pagination and retrieve the projects
		$projects = $query->offset($pagination->offset)
			->limit($pagination->limit)
			->all();
		
		$data['projects'] = $projects;
		$data['pagination'] = $pagination;
		
		return $data;
	}
	
	public static function getPopular()
	{
		return Project::find()->orderBy('viewed desc')->limit(3)->all();
	}
	
	public static function getRecent()
	{
		return Project::find()->orderBy('date asc')->limit(4)->all();
	}
	
	public function getAuthor()
	{
		return $this->hasOne(User::className(), ['id'=>'user_id']);
	}
	
	public function viewedCounter()
	{
		$this->viewed += 1;
		return $this->save(false);
	}

}
