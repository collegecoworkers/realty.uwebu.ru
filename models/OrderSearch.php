<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
	public function rules()
	{
		return [
			[['email', 'phone'], 'string'],
			[['id', 'user_id', 'project_id'], 'integer'],
			[['date'], 'date'],
		];
	}

	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	public function search($params)
	{
		$query = Order::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'user_id' => $this->user_id,
			'project_id' => $this->project_id,
			'email' => $this->email,
			'phone' => $this->phone,
		]);

		return $dataProvider;
	}

	public function getUserName(){
		return User::findIdentity($this->id)->name;
	}

	public function getProjectTitle(){
		return Project::findIdentity($this->id)->title;
	}

}
