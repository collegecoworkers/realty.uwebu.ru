<?php

namespace app\controllers;

use app\models\Project;

use Yii;
use yii\data\Pagination;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Order;

class SiteController extends Controller {

	public function behaviors() {
		return [];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class' => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionIndex() {
		$data = Project::getAll(12);

		return $this->render('index',[
			'projects'=>$data['projects'],
		]);
	}

	public function actionProjects($id = null) {
		$data = Project::getAll(12);
		$recent = Project::getRecent();
		// $popular = Project::getPopular();

		return $this->render('projects',[
			'projects'=>$data['projects'],
			'pagination'=>$data['pagination'],
			'recent'=>$recent,
		]);
	}

	public function actionView($id) {

		$project = Project::findOne($id);
		// $popular = Project::getPopular();
		$recent = Project::getRecent();
		$order = new Order();

		$project->viewedCounter();
		
		if(Yii::$app->request->isPost && !Yii::$app->user->isGuest){

			$order->load(Yii::$app->request->post());

			$order->user_id = Yii::$app->user->id;

			$order->email = Yii::$app->request->post()['Order']['email'];
			$order->phone = Yii::$app->request->post()['Order']['phone'];

			$order->project_id = $project->id;
			$order->date = date('Y-m-d');

			$order->save();

			return $this->redirect(['site/view','id'=>$id]);
		}

		return $this->render('single',[
			'project'=>$project,
			'recent'=>$recent,
			'categories'=>$categories,
			'order'=>$order,
		]);
	}

}
