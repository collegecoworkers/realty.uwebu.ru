<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- banner -->
<div class="inside-banner">
  <div class="container"> 
    <h2><?= $this->title ?></h2>
  </div>
</div>
<!-- banner -->

<div class="container">
  <div class="spacer">
    <div class="row register">
      <div class="col-lg-6 col-lg-offset-3 col-sm-6 col-sm-offset-3 col-xs-12 ">
				<?php 
					$form = ActiveForm::begin([
						'id' => 'login-form',
					]);
				?>

				<?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>

				<?= $form->field($model, 'email')->textInput() ?>

				<?= $form->field($model, 'password')->passwordInput() ?>

						<?= Html::submitButton('Отправить', ['class' => 'btn btn-success' ]) ?>

				<?php ActiveForm::end(); ?>
      </div>
    </div>
  </div>
</div>
