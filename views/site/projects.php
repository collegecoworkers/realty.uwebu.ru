<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<!-- banner -->
<div class="inside-banner">
	<div class="container"> 
		<h2>Продажа</h2>
	</div>
</div>
<!-- banner -->

<div class="container">
	<div class="properties-listing spacer">
		<div class="row">
			<div class="col-lg-3 col-sm-4 ">
				<?= $this->render('/partials/sidebar', ['projects'=>$recent,]);?>
			</div>
			<div class="col-lg-9 col-sm-8">
				<div class="row">
					<?php foreach($projects as $item):?>
						<!-- properties -->
						<div class="col-lg-4 col-sm-6">
							<div class="properties">
								<div class="image-holder">
									<img src="<?= $item->getImage() ?>" class="img-responsive" alt="properties">
									<!-- <div class="status sold">Sold</div> -->
								</div>
								<h4><a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><?= $item->title ?></a></h4>
								<p class="price">Цена: $<?= $item->price ?></p>
								<!-- <div class="listing-detail"><span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">5</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> </div> -->
								<a class="btn btn-primary" href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>">Подробнее</a>
							</div>
						</div>
						<!-- properties -->
					<?php endforeach ?>
					<!-- properties -->
					<div class="center">
						<?php
							echo LinkPager::widget([
								'pagination' => $pagination,
							]);
						?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
