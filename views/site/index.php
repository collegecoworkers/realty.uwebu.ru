<?php
use yii\helpers\Url;
use yii\widgets\LinkPager;
?>

<div class="">
  <div id="slider" class="sl-slider-wrapper">
    <div class="sl-slider">
    	<?php foreach($projects as $item):?>
      <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
        <div class="sl-slide-inner">
          <div class="bg-img" style="background-image: url(<?= $item->getImage() ?>); "></div>
          <h2><a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><?= $item->title ?></a></h2>
          <blockquote>
            <p class="location"><?= $item->getDate() ?></p>
            <p><?= $item->description ?></p>
            <cite>$ <?= $item->price ?></cite>
          </blockquote>
        </div>
      </div>
			<?php endforeach;?>
    </div>
    <!-- /sl-slider -->
    <nav id="nav-dots" class="nav-dots">
    	<?php for ($i=0; $i < count($projects); $i++) { 
	      echo '<span class="'.(($i == 0) ? "nav-dot-current" : "") .'"></span>';
    	} ?>
    </nav>
  </div>
  <!-- /slider-wrapper -->
</div>

<div class="container">
  <div class="properties-listing spacer">
    <a href="<?= Url::toRoute(['/site/projects']) ?>" class="pull-right viewall">Посмотреть все</a>
    <h2>Выгодные предложения</h2>
    <div id="owl-example" class="owl-carousel">
    <?php foreach($projects as $item):?>
      <div class="properties">
        <div class="image-holder">
          <img src="<?= $item->getImage() ?>" class="img-responsive" alt="properties"/>
          <!-- <div class="status sold">Sold</div> -->
        </div>
        <h4><a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><?= $item->title ?></a></h4>
        <p class="price">Цена: $<?= $item->price ?></p>
        <div class="listing-detail">
          <!-- <span data-toggle="tooltip" data-placement="bottom" data-original-title="Bed Room">5</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Living Room">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Parking">2</span> <span data-toggle="tooltip" data-placement="bottom" data-original-title="Kitchen">1</span> -->
        </div>
        <a class="btn btn-primary" href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>">Подробнее</a>
      </div>
    <?php endforeach;?>
    </div>
  </div>
</div>