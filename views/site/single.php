<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\User;

?>
<!-- banner -->
<div class="inside-banner">
	<div class="container">
		<h2>Купить</h2>
	</div>
</div>
<!-- banner -->
<div class="container">
	<div class="properties-listing spacer">
		<div class="row">
			<div class="col-lg-3 col-sm-4 hidden-xs">
				<?= $this->render('/partials/sidebar', ['projects'=>$recent,]);?>
			</div>
			<div class="col-lg-9 col-sm-8 ">
				<h2><?= $project->title ?></h2>
				<div class="row">
					<div class="col-lg-8">
						<div class="property-images">
							<img src="<?= $project->getImage();?>" class="properties" alt="properties" />
						</div>
						<div class="spacer">
							<h4><span class="glyphicon glyphicon-th-list"></span> Описание</h4>
							<?= $project->content ?>
						</div>
					</div>
					<div class="col-lg-4">
						<div class="col-lg-12  col-sm-6">
							<div class="property-info">
								<p class="price">$ <?= $project->price ?></p>
								<p class="area"><span class="glyphicon glyphicon-map-marker"></span> <?= $project->address ?></p>
								<div class="profile">
									<span class="glyphicon glyphicon-user"></span> Агент
									<p><?= User::findIdentity($project->user_id)->name ?></p>
								</div>
							</div>
						</div>
						<div class="col-lg-12 col-sm-6 ">
							<?php if(!Yii::$app->user->isGuest): ?>
							<div class="enquiry">
								<h6><span class="glyphicon glyphicon-envelope"></span>Отправить заявку</h6>
								<?php $form = ActiveForm::begin();?>

								<?= $form->field($order, 'email')->textInput(['autofocus' => true]) ?>

								<?= $form->field($order, 'phone')->textInput() ?>

								<?= Html::submitButton('Отправить', ['class' => 'btn btn-success' ]) ?>

								<?php ActiveForm::end(); ?>
							</div>
						<?php else: ?>
							<aside>
								<h6><span class="glyphicon glyphicon-envelope"></span>Отправить заявку</h6>
								<div class="block_leave_comment_1" style="padding-top: 0;">
									<p >Чтобы отправить заявку нужно авторизоваться</p>
									<a href="<?= Url::toRoute(['auth/login'])?>" class="btn btn-primary">Войти</a>
								</div>
							</aside>
						<?php endif ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>