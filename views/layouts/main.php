<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Category;

PublicAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">

	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
  
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

 	<link rel="stylesheet" href="/public/bootstrap/css/bootstrap.css" />
  <link rel="stylesheet" href="/public/style.css"/>
  <script src="/public/jquery-1.9.1.min.js"></script>
	<script src="/public/bootstrap/js/bootstrap.js"></script>
  <script src="/public/script.js"></script>

<!-- Owl stylesheet -->
<link rel="stylesheet" href="/public/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="/public/owl-carousel/owl.theme.css">
<script src="/public/owl-carousel/owl.carousel.js"></script>
<!-- Owl stylesheet -->

<!-- slitslider -->
    <link rel="stylesheet" type="text/css" href="/public/slitslider/css/style.css" />
    <link rel="stylesheet" type="text/css" href="/public/slitslider/css/custom.css" />
    <script type="text/javascript" src="/public/slitslider/js/modernizr.custom.79639.js"></script>
    <script type="text/javascript" src="/public/slitslider/js/jquery.ba-cond.min.js"></script>
    <script type="text/javascript" src="/public/slitslider/js/jquery.slitslider.js"></script>
<!-- slitslider -->

</head>
<body>
<?php $this->beginBody() ?>

<?= $this->render('/partials/header');?>

<?= $content ?>

<?= $this->render('/partials/footer');?>

<?php $this->endBody() ?>
<?php $this->endPage() ?>
