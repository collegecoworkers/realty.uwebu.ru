<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="hot-properties hidden-xs">
	<h4>Новинки</h4>
	<?php foreach($projects as $item):?>
	<div class="row">
		<div class="col-lg-4 col-sm-5"><img src="<?= $item->getImage() ?>" class="img-responsive img-circle" alt="properties"></div>
		<div class="col-lg-8 col-sm-7">
			<h5><a href="<?= Url::toRoute(['site/view', 'id'=>$item->id]);?>"><?= $item->title ?></a></h5>
			<p class="price">$<?= $item->price ?></p>
		</div>
	</div>
	<?php endforeach ?>
</div>