<?php
use yii\helpers\Url;
?>
<!-- Header Starts -->
<div class="navbar-wrapper">

        <div class="navbar-inverse" role="navigation">
          <div class="container">
            <div class="navbar-header">


              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>

            </div>


            <!-- Nav Starts -->
            <div class="navbar-collapse  collapse">
              <ul class="nav navbar-nav navbar-right">
              	<li class=""><a href="<?= Url::toRoute(['/']) ?>">Главная</a></li>
								<li class=""><a href="<?= Url::toRoute(['/site/projects']) ?>">Продажа</a></li>
              	<?php if(Yii::$app->user->isGuest):?>
              		<li><a href="<?= Url::toRoute(['/auth/login'])?>">Вход</a></li>
              		<li><a href="<?= Url::toRoute(['/auth/signup'])?>">Регистрация</a></li>
              	<?php else: ?>
              		<?php if(Yii::$app->user->identity->isAdmin && false): ?>
              			<li><a href="<?= Url::toRoute(['/admin'])?>">Админка</a></li>
              		<?php endif ?>
              		<li><a href="<?= Url::toRoute(['/user/my-projects'])?>">Мои обьявления</a></li>
              		<li><a href="<?= Url::toRoute(['/auth/logout'])?>">Выход <?= ' (' . Yii::$app->user->identity->name . ')' ?></a></li>
              	<?php endif;?>
              </ul>
            </div>
            <!-- #Nav Ends -->

          </div>
        </div>

    </div>
<!-- #Header Starts -->

<div class="container">

<!-- Header Starts -->
<div class="header">
<a href="<?= Url::toRoute(['/']) ?>"><img src="/public/images/logo.png" alt="Logo"></a>
</div>
<!-- #Header Starts -->
</div>