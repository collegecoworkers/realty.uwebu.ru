<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use app\models\Order;
use app\models\OrderSearch;

$this->title = 'Мои обьявления';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
	
<div class="block_general_title_1">
	<h1><?= $this->title ?></h1>
</div>
<div id="content" class="sidebar_right">
	<div class="inner">

		<p>
			<?= Html::a('Создать обьявление', ['create'], ['class' => '']) ?>
		</p>
		<?= 
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					// ['class' => 'yii\grid\SerialColumn'],

					'id',
					'title',
					'price',
					'viewed',
					[
						'label' => 'Заявки',
						'format' => 'raw',
						'value' => function($data){
							return Html::a(count(Order::getByProject($data->id)), ['/user/orders-for-me/', 'id' => $data->id]);
						},
					],

					['class' => 'yii\grid\ActionColumn'],
				],
			]); ?>

	</div>
</div>
</div>
