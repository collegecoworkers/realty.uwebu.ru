<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use yii\bootstrap\ActiveForm;
use yii\grid\GridView;
use app\models\Order;
use app\models\OrderSearch;

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
	
<div class="block_general_title_1">
	<h1><?= $this->title ?></h1>
</div>
<div id="content" class="sidebar_right">
	<div class="inner">

		<?= 
			GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'columns' => [
					// ['class' => 'yii\grid\SerialColumn'],

					'id',
					'UserName',
					'ProjectTitle',
					'email',
					'phone',

					// ['class' => 'yii\grid\ActionColumn'],
				],
			]); ?>

	</div>
</div>
</div>
